function initMap() {
    var map = new google.maps.Map(document.getElementById('googleMap'), {
        center: {lat: 50.278912, lng: 19.561781},
        zoom: 18,
        styles: [{
            "featureType": "all",
            "elementType": "labels",
            "stylers": [{"visibility": "on"}]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{"visibility": "on"}, {"color": "#f3f4f4"}]
        }, {
            "featureType": "landscape.man_made",
            "elementType": "geometry",
            "stylers": [{"weight": 0.9}, {"visibility": "on"}]
        }, {
            "featureType": "poi.park",
            "elementType": "geometry.fill",
            "stylers": [{"visibility": "on"}, {"color": "#83cead"}]
        }, {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{"visibility": "on"}, {"color": "#ffffff"}]
        }, {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [{"visibility": "on"}, {"color": "#83cead"}, {"weight": 0.5}]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{"visibility": "on"}, {"color": "#fee379"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "all",
            "stylers": [{"visibility": "on"}, {"color": "#fee379"}]
        }, {"featureType": "water", "elementType": "all", "stylers": [{"visibility": "on"}, {"color": "#7fc8ed"}]}]
    });

    var marker = getMarker(50.278982, 19.561898);

    marker.setMap(map);
}

function getMarker(x, y) {
    return new google.maps.Marker({
        position: new google.maps.LatLng(x, y),
        icon: 'http://localhost:63342/Logopeda%20Olkusz/img/map-marker.png'
    });
}

$(document).ready(function () {
    initMap();
});
