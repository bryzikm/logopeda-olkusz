function bootstrapCarousel() {
    $('.carousel').carousel({
        interval: 20000
    });
}

$(document).ready(function () {

    bootstrapCarousel();

    $(window).scroll( function() {

        if($(window).scrollTop() > 145) {
            $('#header').addClass('fixed');
        } else {
            $('#header').removeClass('fixed');
        }

    });

    $('#gallerySlick').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        slidesToShow: 3,
        centerMode: true,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 1
            }
        }]
    });
});